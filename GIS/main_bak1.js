const map = L.map('map', {
  crs: L.CRS.EPSG4326
}).setView([30.25, 120.15], 12);

// 国家天地图矢量
L.tileLayer('http://t0.tianditu.com/vec_c/wmts?tk=85b88ce10c15f390ee75bf571688b3b7&layer=vec&style=default&tilematrixset=c&Service=WMTS&Request=GetTile&Version=1.0.0&Format=tiles&TileMatrix={z}&TileCol={x}&TileRow={y}', {
  maxZoom: 20,
  tileSize: 256,
  zoomOffset: 1
}).addTo(map)

// 国家天地图矢量注记
L.tileLayer('http://t0.tianditu.com/cva_c/wmts?tk=85b88ce10c15f390ee75bf571688b3b7&layer=cva&style=default&tilematrixset=c&Service=WMTS&Request=GetTile&Version=1.0.0&Format=tiles&TileMatrix={z}&TileCol={x}&TileRow={y}', {
  maxZoom: 20,
  tileSize: 256,
  zoomOffset: 1
}).addTo(map)
/*
// 浙江省天地图矢量
L.tileLayer('http://srv1.zjditu.cn/ZJEMAP_2D/wmts?layer=TDT_ZJEMAP&style=default&tilematrixset=default028mm&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image%2Fjpgpng&TileMatrix={z}&TileCol={x}&TileRow={y}', {
  minZoom: 7,
  maxZoom: 20,
  tileSize: 256,
  zoomOffset: 1
}).addTo(map)

// 浙江天地图矢量注记
L.tileLayer('http://srv1.zjditu.cn/ZJEMAPANNO_2D/wmts?layer=TDT_ZJEMAPANNO&style=default&tilematrixset=default028mm&Service=WMTS&Request=GetTile&Version=1.0.0&Format=image%2Fjpgpng&TileMatrix={z}&TileCol={x}&TileRow={y}', {
  minZoom: 7,
  maxZoom: 20,
  tileSize: 256,
  zoomOffset: 1
}).addTo(map)*/