#! /bin/bash
echo -e "\n*****************IOT_MQTT_Profile Installtion Started*****************\n"

echo -e "\n*****************MQTT Broker Installtion Started*****************\n"
echo "`./emqx_broker_installation.sh`"
echo -e "\n*****************MQTT Broker Installtion Completed*****************\n"

echo -e "\n*****************Fledge Installtion Started*****************\n"
echo "`./fledge_installation.sh`"
echo -e "\n*****************Fledge Installtion Completed*****************\n"

echo -e "\n*****************ZMQ Installtion Started*****************\n"
echo "`./zeromq.sh`"
echo -e "\n*****************ZMQ Installtion Completed*****************\n"

echo -e "\n*****************Fledge Adaptor Installtion Started*****************\n"
echo "`./fledge_adaptor.sh`"
echo -e "\n*****************Fledge Adaptor Installtion Completed*****************\n"

echo -e "\n*****************Kuiper Installtion Started*****************\n"
echo "`./kuiper_installation.sh`"
echo -e "\n*****************Kuiper Installtion Completed*****************\n"

sleep 5

echo -e "\n*****************Fledge Config Started*****************\n"
echo "`./fledge_config.sh`"
echo -e "\n*****************Fledge Config Completed*****************\n"

echo -e "\n*****************Kuiper Config Started*****************\n"
echo "`./kuiper_config.sh`"
echo -e "\n*****************Kuiper Config Completed*****************\n"

echo -e "\n*****************IOT_MQTT_Profile Installtion Completed*****************\n"
