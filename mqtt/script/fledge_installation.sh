#! /bin/bash

echo -e "\n************* Server is getting latest updates ************\n"
sudo apt-get update
sudo apt-get upgrade
sudo apt-get update

echo -e "\n************* Install Fledge from PKG Repo ************\n"
wget -q -O - http://archives.fledge-iot.org/KEY.gpg | sudo apt-key add -
sudo add-apt-repository "deb http://archives.fledge-iot.org/latest/ubuntu1804/x86_64/ / "
sudo apt update

echo -e "\n************* Installing Base Fledge and GUI ************\n"
sudo apt -y install fledge fledge-gui fledge-south-sinusoid

echo -e "\n************* Starting Fledge  ************\n"
sudo /usr/local/fledge/bin/fledge start

