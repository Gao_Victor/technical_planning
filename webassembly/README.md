### 编译器

Emscripten：https://emscripten.org/

### 安装步骤


```
# Install Python
sudo apt-get install python2.7

sudo apt-get install cmake

sudo apt-get install default-jre

git clone https://github.com/emscripten-core/emsdk.git

cd emsdk

git pull

./emsdk install latest

./emsdk activate latest

source ./emsdk_env.sh
```


### 编译
修改Main.c宏


```
#ifdef __EMSCRIPTEN__
    #include <emscripten.h>
#endif
```

### 编译命令
`emcc  -o app.html *.c  -Wall -g -lm  -s USE_SDL=2`
