
#!/bin/bash

. ./config.sh


FLEDGE_SVC_URL="fledge/service"

### Install plugin mqtt-readngs
echo -e "\n**********Installing MQTT-Readings Plugin ***********\n"
curl -X POST -H "Content-Type: application/json" \
   -d '{"format":"repository","name":"fledge-south-mqtt-readings","version":""}'\
    $FLEDGE_HOST/fledge/plugins

sleep 10s
echo -e "\n*********** Config MQTT South device ***********\n"
### Subscribe south mqtt service to certain broker
curl -X POST -H "Content-Type: Application/json" -d '{"name": "'"$SERVICE_NAME"'", "type": "south", "address": "'"$PUB_SUB_HOST"'","plugin" :"mqtt-readings","enabled":true,"config": {"brokerHost": {"value": "'"$MQTT_BROKER_HOST"'"},"brokerPort": {"value": "'"$BROKER_PORT"'"},"topic": {"value": "'"$TOPIC"'"},"assetName":{"value":"'"$ASSET_NAME"'"}}}' $FLEDGE_HOST/$FLEDGE_SVC_URL

sleep 10s
### Install plugin http-north
echo -e "\n**********Installing North-Http Plugin ***********\n"
output=$(curl -v -X POST -H "Content-Type: application/json" \
   -d '{"format":"repository","name":"fledge-north-http-north","version":""}'\
   $FLEDGE_HOST/fledge/plugins)

sleep 10s
# Subscribe North http service to certain broker
echo -e "\n*********** Config North Http Interface ***********\n"
curl -X POST -H "Content-Type: Application/json" -d '{"name":"kuiper01","plugin":"http_north","type":"north","config":{"url": {"value": "http://'"$FLEDGE_ADAPTOR_IP"':'"$FLEDGE_ADAPTOR_PORT"'/sensor-reading"}}, "enabled":true}' $FLEDGE_HOST/fledge/service



