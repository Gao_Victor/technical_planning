# run in sudo
# Before installing, make sure you have installed all the needed packages
sudo apt-get install -y libtool pkg-config build-essential autoconf automake
sudo apt-get install -y libzmq3-dev

# Install libsodium
sudo apt-get install libsodium-dev

cd /opt

# Install zeromq
# latest version as of this post is 4.1.2
echo "********* Building and installing ZMQ **********"
wget http://download.zeromq.org/zeromq-4.1.2.tar.gz
tar -xvf zeromq-4.1.2.tar.gz
cd zeromq-4.1.2
./configure
make
sudo make install

sudo apt-get install -y php-dev -y php-pear
sudo pecl install zmq-beta
