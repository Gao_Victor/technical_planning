#!/bin/bash


##download kuiper code
echo -e "\n******** Downloading ekuiper 1.3.0 ********\n"
git clone --branch 1.3.0 https://github.com/lf-edge/ekuiper.git

##build fledge adaptor code
cd ekuiper

echo -e "\n******** Building ekuiper  ********\n"
make

echo -e "\n******** Building ZMQ Plugin  ********\n"
go build -trimpath -modfile extensions.mod --buildmode=plugin -o ./_build/kuiper-1.3.0-linux-amd64/plugins/sources/Zmq.so extensions/sources/zmq/zmq.go
cd _build/kuiper-1.3.0-linux-amd64

echo -e "\n******** Starting Kuiper with ZMQ plugin  ********\n"
./bin/kuiperd </dev/null &>/dev/null &
