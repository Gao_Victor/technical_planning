#! /bin/bash

# uninstall 
## uninstall containers (emx, fledge-adaptor)
echo -e "\n***********IOT_MQTT_Profile Uninstallation Started***********\n"

docker kill emqx
docker rm emqx
echo -e "\n*********** MQTT Broker Uninstalled ***********\n"

docker kill fledgeadaptor
docker rm fledgeadaptor
echo -e "\n********** Fledge Adaptor Uninstalled***********\n"

#uninstall fledge
/usr/local/fledge/bin/fledge stop
sudo apt-get --purge remove fledge fledge-gui fledge-south-sinusoid -y
rm -rf /usr/local/fledge

sudo add-apt-repository --remove "deb http://archives.fledge-iot.org/latest/ubuntu1804/x86_64/ / "
echo -e "\n***********Fledge Uninstallated***********\n"

#ekuiper
pkill -9 kuiperd
rm -rf ekuiper
echo -e "\n***********Kuiper Uinstalled ***********\n"

#zmq Installaion
apt-get remove --auto-remove libzmq-dev -y
apt-get remove --auto-remove libzmq3-dev -y
echo -e "\n***********ZMQ Uninstalled ***********\n"

echo -e "\n***********IOT_MQTT_Profile uninstallation Completed***********\n"



