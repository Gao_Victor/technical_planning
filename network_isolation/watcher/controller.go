package watcher

import (
	"encoding/json"
	"fmt"
	"github.com/intel/multus-cni/types"
	netattachdef "github.com/k8snetworkplumbingwg/network-attachment-definition-client/pkg/apis/k8s.cni.cncf.io/v1"
	clientset "github.com/k8snetworkplumbingwg/network-attachment-definition-client/pkg/client/clientset/versioned"
	informers "github.com/k8snetworkplumbingwg/network-attachment-definition-client/pkg/client/informers/externalversions/k8s.cni.cncf.io/v1"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/apimachinery/pkg/util/wait"
	coreinformers "k8s.io/client-go/informers/core/v1"
	"k8s.io/client-go/kubernetes"
	corelisters "k8s.io/client-go/listers/core/v1"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/record"
	"k8s.io/client-go/util/workqueue"
	"k8s.io/kubernetes/pkg/api/v1/endpoints"
	podutil "k8s.io/kubernetes/pkg/api/v1/pod"
	"time"
)

type Controller struct {
	clientSet           kubernetes.Interface //Client to get information of k8s resources, svcs, pods etc.
	netWatcherClientSet clientset.Interface  //Client to get information of custom net-attachment

	netAttachDefsSynced cache.InformerSynced

	podsLister corelisters.PodLister
	podsSynced cache.InformerSynced

	serviceLister  corelisters.ServiceLister
	servicesSynced cache.InformerSynced

	endpointsLister corelisters.EndpointsLister
	endpointsSynced cache.InformerSynced

	workqueue workqueue.RateLimitingInterface
	recorder record.EventRecorder
}

const (
	selectionsKey       = "k8s.v1.cni.cncf.io/networks"
	statusesKey         = "k8s.v1.cni.cncf.io/networks-status"
	controllerAgentName = "k8s-net-attach-def-controller"
)

func NewNetworkController(
	clientSet kubernetes.Interface,
	netWatcherClientSet clientset.Interface,
	netWatcherInformer informers.NetworkAttachmentDefinitionInformer,
	svcInformer coreinformers.ServiceInformer,
	podInformer coreinformers.PodInformer,
	epInformer coreinformers.EndpointsInformer) *Controller {
	// Records events
	recorder := createRecorder(clientSet, controllerAgentName)

	NetworkController := &Controller{
		clientSet:           clientSet,
		netWatcherClientSet: netWatcherClientSet,
		netAttachDefsSynced: netWatcherInformer.Informer().HasSynced,
		podsLister:          podInformer.Lister(),
		podsSynced:          podInformer.Informer().HasSynced,
		serviceLister:       svcInformer.Lister(),
		servicesSynced:      svcInformer.Informer().HasSynced,
		endpointsLister:     epInformer.Lister(),
		endpointsSynced:     epInformer.Informer().HasSynced,
		workqueue:           workqueue.NewNamedRateLimitingQueue(workqueue.DefaultControllerRateLimiter(), "Endpoints"),
		recorder:            recorder,
	}
	//TODO: May support adding network attachment addition. Currently network definition must be added before.
	netWatcherInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		DeleteFunc: NetworkController.handleNetAttachDefDeleteEvent,
	})

	/* setup handlers for endpoints events */
	epInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc:    NetworkController.handleEndpointEvent,
		UpdateFunc: NetworkController.updateEndPoint,
		DeleteFunc: NetworkController.handleEndpointEvent,
	})

	/* setup handlers for services events */
	svcInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc:    NetworkController.handleServiceEvent,
		UpdateFunc: NetworkController.updateSvc,
		DeleteFunc: NetworkController.handleServiceEvent,
	})

	podInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		AddFunc:    NetworkController.handlePodEvent,
		UpdateFunc: NetworkController.updatePod,
		DeleteFunc: NetworkController.handlePodEvent,
	})

	return NetworkController
}


func (c *Controller) handleEndpointEvent(obj interface{}) {
	ep := obj.(*corev1.Endpoints)

	// get service associated with endpoints instance
	svc, err := c.serviceLister.Services(ep.GetNamespace()).Get(ep.GetName())
	if err != nil {
		// errors are returned for service-less endpoints such as kube-scheduler and kube-controller-manager
		return
	}

	c.handleServiceEvent(svc)
}

func (c *Controller) updatePod(old, new interface{}) {
	oldPod := old.(*corev1.Pod)
	newPod := new.(*corev1.Pod)
	if oldPod.ResourceVersion == newPod.ResourceVersion || newPod.ObjectMeta.DeletionTimestamp != nil {
		return
	}
	c.handlePodEvent(new)
}

func (c *Controller) handlePodEvent(obj interface{}) {
	pod, ok := obj.(*corev1.Pod)
	if !ok {
		return
	}

	// if no network annotation discard
	_, ok = pod.GetAnnotations()[selectionsKey]
	if !ok {
		log.Info("skipping pod event: network annotations missing")
		return
	}

	// if not behind any service discard
	services, err := c.serviceLister.GetPodServices(pod)
	if err != nil {
		log.Info("skipping pod event: %s", err)
		return
	}
	for _, svc := range services {
		c.handleServiceEvent(svc)
	}
}

func (c *Controller) handleServiceEvent(obj interface{}) {
	key, err := cache.MetaNamespaceKeyFunc(obj)
	if err != nil {
		runtime.HandleError(err)
		return
	}
	c.workqueue.AddRateLimited(key)
}

func (c *Controller)updateEndPoint (old, new interface{}) {
	oldEp := old.(*corev1.Endpoints)
	newEp := new.(*corev1.Endpoints)
	if oldEp.ResourceVersion == newEp.ResourceVersion {
		return
	}
	c.handleEndpointEvent(new)
}

func (c *Controller)updateSvc (old, new interface{}) {
	oldSvc := old.(*corev1.Service)
	newSvc := new.(*corev1.Service)
	if oldSvc.ResourceVersion == newSvc.ResourceVersion {
		return
	}
	c.handleServiceEvent(new)
}

func (c *Controller) Run(stopChan <-chan struct{}) {
	log.Infof("starting network controller")
	defer c.workqueue.ShutDown()

	if ok := cache.WaitForCacheSync(stopChan, c.netAttachDefsSynced, c.endpointsSynced, c.servicesSynced, c.podsSynced); !ok {
		log.Fatalf("failed waiting for caches to sync")
	}

	go wait.Until(c.worker, time.Second, stopChan)

	<-stopChan
	log.Infof("shutting down network controller")
	return
}

func (c *Controller) worker() {
	for c.processNextWorkItem() {
	}
}

func (c *Controller) processNextWorkItem() bool {
	key, shouldQuit := c.workqueue.Get()
	if shouldQuit {
		return false
	}
	defer c.workqueue.Done(key)

	err := c.syncHandler(key.(string))
	if err != nil {
		log.Infof("sync aborted: %s", err)
	}

	return true
}

func (c *Controller) syncHandler(key string) error {
	ns, name, err := cache.SplitMetaNamespaceKey(key)
	if err != nil {
		return err
	}
	log.Infof("ns, name: %s %s\n", ns, name)
	log.Infof("key: %s\n", key)

	svc, err := c.serviceLister.Services(ns).Get(name)
	if err != nil {
		return err
	}

	// read network annotations from the service
	annotations := getNetworkAnnotations(svc)
	if len(annotations) == 0 {
		return errors.New("no network annotations")
	}
	log.Infof("service network annotation found: %v", annotations)
	networks, err := parsePodNetworkSelections(annotations, ns)
	if err != nil {
		return err
	}
	if len(networks) > 1 {
		msg := fmt.Sprintf("multiple network selections in the service spec are not supported")
		log.Warningf(msg)
		return errors.New(msg)
	}

	// get pods matching service selector
	selector := labels.Set(svc.Spec.Selector).AsSelector()
	pods, err := c.podsLister.List(selector)
	if err != nil {
		// no selector or no pods running
		log.Info("error listing pods matching service selector: %s", err)
		return err
	}

	// get endpoints of the service
	ep, err := c.endpointsLister.Endpoints(ns).Get(name)
	if err != nil {
		log.Info("error getting service endpoints: %s", err)
		return err
	}

	subsets := make([]corev1.EndpointSubset, 0)

	for _, pod := range pods {
		addresses := make([]corev1.EndpointAddress, 0)
		ports := make([]corev1.EndpointPort, 0)

		networksStatus := make([]types.NetworkStatus, 0)
		err := json.Unmarshal([]byte(pod.Annotations[statusesKey]), &networksStatus)
		if err != nil {
			log.Warningf("error reading pod networks status: %s", err)
			continue
		}
		// find networks used by pod and match network annotation of the service
		for _, status := range networksStatus {
			if isInNetworkSelectionElementsArray(status.Name, networks) {
				log.Infof("processing pod %s/%s: found network %s interface %s with IP addresses %s",
					pod.Namespace, pod.Name, annotations, status.Interface, status.IPs)
				// all IPs of matching network are added as endpoints
				for _, ip := range status.IPs {
					epAddress := corev1.EndpointAddress{
						IP:       ip,
						NodeName: &pod.Spec.NodeName,
						TargetRef: &corev1.ObjectReference{
							Kind:            "Pod",
							Name:            pod.GetName(),
							Namespace:       pod.GetNamespace(),
							ResourceVersion: pod.GetResourceVersion(),
							UID:             pod.GetUID(),
						},
					}
					addresses = append(addresses, epAddress)
				}
			}
		}
		for i := range svc.Spec.Ports {
			// check whether pod has the ports needed by service and add them to endpoints if so
			portNumber, err := podutil.FindPort(pod, &svc.Spec.Ports[i])
			if err != nil {
				log.Infof("Could not find pod port for service %s/%s: %s, skipping...", svc.Namespace, svc.Name, err)
				continue
			}

			port := corev1.EndpointPort{
				Port:     int32(portNumber),
				Protocol: svc.Spec.Ports[i].Protocol,
				Name:     svc.Spec.Ports[i].Name,
			}
			ports = append(ports, port)
		}
		subset := corev1.EndpointSubset{
			Addresses: addresses,
			Ports:     ports,
		}
		subsets = append(subsets, subset)
	}

	ep.SetOwnerReferences(
		[]metav1.OwnerReference{
			*metav1.NewControllerRef(svc, schema.GroupVersionKind{
				Group:   corev1.SchemeGroupVersion.Group,
				Version: corev1.SchemeGroupVersion.Version,
				Kind:    "Service",
			}),
		},
	)

	ep.Subsets = endpoints.RepackSubsets(subsets)

	// update endpoints resource
	_, err = c.clientSet.CoreV1().Endpoints(ep.Namespace).Update(ep)
	if err != nil {
		log.Errorf("error updating endpoint: %s", err)
		return err
	}

	log.Info("endpoint updated successfully")
	return nil
}
func (c *Controller) handleNetAttachDefDeleteEvent(obj interface{}) {
	netAttachDef, ok := obj.(metav1.Object)
	if ok {
		name := netAttachDef.GetName()
		namespace := netAttachDef.GetNamespace()
		log.Infof("handling deletion of %s/%s", namespace, name)
		pods, _ := c.podsLister.Pods("").List(labels.Everything())
		/* check whether net-attach-def requested to be removed is still in use by any of the pods */
		for _, pod := range pods {
			netAnnotations, ok := pod.ObjectMeta.Annotations[selectionsKey]
			if !ok {
				continue
			}
			podNetworks, err := parsePodNetworkSelections(netAnnotations, pod.ObjectMeta.Namespace)
			if err != nil {
				continue
			}
			for _, net := range podNetworks {
				if net.Namespace == namespace && net.Name == name {
					log.Infof("pod %s uses net-attach-def %s/%s which needs to be recreated\n", pod.ObjectMeta.Name, namespace, name)
					/* check whether the object somehow still exists */
					_, err := c.netWatcherClientSet.K8sCniCncfIoV1().
						NetworkAttachmentDefinitions(netAttachDef.GetNamespace()).
						Get(netAttachDef.GetName(), metav1.GetOptions{})
					if err != nil {
						/* recover deleted object */
						recovered := obj.(*netattachdef.NetworkAttachmentDefinition).DeepCopy()
						recovered.ObjectMeta.ResourceVersion = "" // ResourceVersion field needs to be cleared before recreating the object
						_, err = c.netWatcherClientSet.
							K8sCniCncfIoV1().
							NetworkAttachmentDefinitions(netAttachDef.GetNamespace()).
							Create(recovered)
						if err != nil {
							log.Errorf("error recreating recovered object: %s", err.Error())
						}
						log.Infof("net-attach-def recovered: %v", recovered)
						return
					}
				}
			}
		}
	}
}