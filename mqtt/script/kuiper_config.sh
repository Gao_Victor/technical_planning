#! /bin/bash

. ./config.sh

###kuiper config:

#### API to add streams with zmq
echo -e "\n********** Config Input Stream in Kuiper **************\n"
curl -X POST -H "Content-Type: Application/json" -d '{"sql":"create stream demo () WITH ( datasource = \"Event\", FORMAT = \"json\", KEY = \"temperature\", TYPE = \"zmq\")"}'\
        $KUIPER_HOST/streams

#### API to add rules with zmqi
echo -e "\n********** Config Rule in Kuiper **************\n"
curl -X POST -H "Content-Type: Application/json" -d '{"id": "rule2","sql": "SELECT * FROM demo WHERE temperature > 30","actions": [{"mqtt": {"server": "tcp://'"$MQTT_BROKER_HOST"':'"$BROKER_PORT"'","topic": "demoSink"}}]}'\
        $KUIPER_HOST/rules
