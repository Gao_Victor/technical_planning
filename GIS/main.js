const map = new L.Map("map", {
	zoom: 4,
	center: [33, 108.5],
	boxZoom: true, 
});

var datacenter = {
	"type" : "FeatureConllection",
	"features" : [
		{
			"geometry" : {
				"type" : "Point",
				"coordinates" : [108.55, 34.32],
				"properties" : {
					"popupContent" : "西安",
					"idStr" : "lygChart"
				}
			},
			"type" : "Feature",
			"id" : 101
		},
		{
			"geometry" : {
				"type" : "Point",
				"coordinates" : [118.8, 32.05],
				"properties" : {
					"popupContent" : "南京",
					"idStr" : "njChart"
				}
			},
			"type" : "Feature",
			"id" : 89
		}
	]
}


var url = 'http://159.138.29.128:8080/geoserver/Victor/wms?service=WMS&version=1.1.0&request=GetMap&layers=Victor:CHN_adm1&styles=&bbox=73.55770111084013,18.159305572509766,134.7739257812502,53.56085968017586&width=768&height=444&srs=EPSG:4326&format=image%2Fpng';
var layer1 = new L.TileLayer(url, {
	subdomains:"1234"
});
//map.addLayer(layer);
//var marker = new L.marker([39.99, 116.3]);
//marker.addTo(map); 
var wmsLayer= L.tileLayer.wms("http://159.138.29.128:8080/geoserver/Victor/wms?", {
	layers: 'Victor:CHN_adm1',//需要加载的图层
	format: 'image/png',//返回的数据格式
	transparent: true,
	styles:'Victor:test'
	//sld_body: 'http://159.138.29.128:8080/home/victor/proviance.sld'
	//crs: L.CRS.EPSG4326
});
console.log(map.getCenter());
map.addLayer(wmsLayer);

var myIcon = L.icon({
    iconUrl: '1925085.svg',
    iconSize: [38, 38],
    iconAnchor: [25, 25],
    popupAnchor: [0, 0],
});


var sld = fetch()

			 
function onEachFeature(feature, layer){
		
		var idStr = feature.geometry.properties.idStr.toString() ;
		var popupContent = '<div style="width:350px;height:300px" id="' + idStr + '"></div>';
		layer.bindPopup(popupContent, {});
		layer.on('popupopen', function(e){
		
			var myChart = echarts.init(document.getElementById(idStr));
			// 指定图表的配置项和数据
			var option = {
				title: {
					text: 'Usage'
				},
				tooltip: {},
				legend: {
					data:['Percentage']
				},
				xAxis: {
					data: ["CPU","MEM","DISK","Network"]
				},
				yAxis: {},
				series: [{
					name: '%',
					type: 'bar',
					data: [5, 20, 36, 10]
				}]
			};
 
			// 使用刚指定的配置项和数据显示图表。
			myChart.setOption(option);
			let overview = document.getElementById('tabledata')
			overview.style.display = 'block'});
	}
	
	L.geoJSON([datacenter], {
 
		onEachFeature: onEachFeature,
		pointToLayer: function (feature, latlng) {
		
		return L.marker(latlng,{icon: myIcon});
		}
	}).addTo(map)


