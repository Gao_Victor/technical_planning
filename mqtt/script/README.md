
# IOT-MQTT-Profile
## Pre-Requisites
1. Docker : Docker version 18.09.0
2. GO setup: If GO setup already avaible then skip this step
   go version go1.14.4 linux/amd64
   Install golang
   wget https://dl.google.com/go/go1.14.4.linux-amd64.tar.gz
   sudo tar -xvf go1.14.4.linux-amd64.tar.gz
   sudo mv go /usr/local

   edit ~./bashrc and add below:
   export GOROOT=/usr/local/go
   export GOPATH=$HOME/go
   export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
   
   source ~/.bashrc 
 
## Pre-Config
Below are some configuration:
| Parameter    | Default value | decription | 
| ------------- | ------------- | ----------|
| MQTT_BROKER_HOST  | Host IP | mqtt broker host IP
| BROKER_PORT  | 1883  | broker Port |
| SERVICE_NAME  |  a valid string | South plugin name |
| ASSET_NAME  |  a valid string | device name |
| FLEDGE_ADAPTOR_IP  | 0.0.0.0  | fledge adaptor service IP |
| FLEDGE_ADAPTOR_PORT  |  9091 | fledge adaptor service Port |
| KUIPER_HOST  | http://localhost:9081  | kuper host url |



## Installation
Update config.sh for any config parameters updates
./setup.sh

## Verification
1. Verify installation
  ./verification.sh
  Check for status of all services for status:
  success as installed and running
  failure as stoped and uninstalled

2. Verify functionality
  install Mosquitto:
  snap install mosquitto
  apt  install mosquitto-clients

  Simulate south MQTT device for testing: Publish data to MQTT on topic Room1/conditions by mosquitto
  ```
  mosquitto_pub -h MQTT_BROKER_IP -p BROKER_PORT -m '{"temperature": 40, "humidity" : 20}' -t Room1/conditions
  ```
  By default config, MQTT_BROKER_IP is local HOST IP and BROKER_PORT is 1883
  
  In other shell, subcribe demoSink topic where Kuiper publish data:
  ```
  mosquitto_sub -h MQTT_BROKER_IP -p BROKER_PORT -t demoSink
  ```
  By default config, MQTT_BROKER_IP is local HOST IP and BROKER_PORT is 1883

  If temperature is above 30, will be send to demoSink others data will be droped.
  
  Open Fledge GUI and check for South and North Plugin and readings statistics
  Fledge GUI can be open on localhost/HOSTIP if remote with default 80 port
  
## UnInstallation
./uninstall.sh
Then call verfication script and check all services are stoped and uninstalled successfully by status as failure.
