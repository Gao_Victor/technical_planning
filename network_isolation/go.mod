module netwatcher

go 1.14

require (
	github.com/containernetworking/cni v0.8.0 // indirect
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/gogo/protobuf v1.3.1 // indirect
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b // indirect
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/google/btree v1.0.0 // indirect
	github.com/google/gofuzz v1.2.0 // indirect
	github.com/googleapis/gnostic v0.5.1 // indirect
	github.com/gregjones/httpcache v0.0.0-20190611155906-901d90724c79 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/imdario/mergo v0.3.11 // indirect
	github.com/intel/multus-cni v0.0.0-20180818113950-86af6ab69fe2
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/k8snetworkplumbingwg/network-attachment-definition-client v0.0.0-20200626054723-37f83d1996bc
	github.com/natefinch/lumberjack v2.0.0+incompatible
	github.com/peterbourgon/diskv v2.0.1+incompatible // indirect
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/crypto v0.0.0-20200728195943-123391ffb6de // indirect
	golang.org/x/net v0.0.0-20200813134508-3edf25e44fcc // indirect
	golang.org/x/time v0.0.0-20200630173020-3af7569d3a1e // indirect
	gopkg.in/inf.v0 v0.9.1 // indirect
	k8s.io/api v0.18.6
	k8s.io/apimachinery v0.18.6
	k8s.io/client-go v11.0.0+incompatible
	k8s.io/klog v1.0.0 // indirect
	k8s.io/kube-openapi v0.0.0-20200811211545-daf3cbb84823 // indirect
	k8s.io/kubernetes v1.11.0-alpha.1.0.20180420161653-9c60fd5242c4
)

replace (
	github.com/containernetworking/cni v0.8.0 => github.com/containernetworking/cni v0.7.0-alpha1
	github.com/googleapis/gnostic v0.5.1 => github.com/googleapis/gnostic v0.0.0-20170729233727-0c5108395e2d
	github.com/k8snetworkplumbingwg/network-attachment-definition-client v0.0.0-20200626054723-37f83d1996bc => github.com/k8snetworkplumbingwg/network-attachment-definition-client v0.0.0-20181121151021-386d141f4c94
	k8s.io/api v0.18.6 => k8s.io/api v0.0.0-20180712090710-2d6f90ab1293
	k8s.io/apimachinery v0.18.6 => k8s.io/apimachinery v0.0.0-20180621070125-103fd098999d
	k8s.io/client-go v11.0.0+incompatible => k8s.io/client-go v0.0.0-20180718001006-59698c7d9724
	k8s.io/kube-openapi v0.0.0-20200811211545-daf3cbb84823  => k8s.io/kube-openapi v0.0.0-20180620173706-91cfa479c814
)