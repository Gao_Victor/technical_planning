#!/bin/bash
NC='\033[0m'
RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
WHITE='\033[1;37m'


printf "%-20s %20s${NC}\n" "ITEM" "STATUS"

result=$( sudo docker ps -a | grep emqx )

if [[ -n "$result" ]]; then
  printf "%-20s ${GREEN}%20s${NC}\n" "emqx" "success"
else
  printf "%-20s ${RED}%20s${NC}\n" "emqx" "failure"
fi


service=fledge

if (( $(ps -ef | grep -v grep | grep $service | wc -l) > 0 ))
then
printf "%-20s ${GREEN}%20s${NC}\n" "fledge" "success"
else
printf "%-20s ${RED}%20s${NC}\n" "fledge" "failure"
fi


result=$( sudo docker ps -a | grep fledgeadaptor )

if [[ -n "$result" ]]; then
  printf "%-20s ${GREEN}%20s${NC}\n" "fledgeadaptor" "success"
else
  printf "%-20s ${RED}%20s${NC}\n" "fledgeadaptor" "failure"
fi


resultk=$( ps aux | grep "[k]uiperd")
if [[ -n "$resultk" ]]; then
  printf "%-20s ${GREEN}%20s${NC}\n" "kuiper" "success"
else
  printf "%-20s ${RED}%20s${NC}\n" "kuiper" "failure"
fi

result=$( ldconfig -p | grep "[z]mq")
if [[ -n "$result" ]]; then
  printf "%-20s ${GREEN}%20s${NC}\n" "zmq" "success"
else
  printf "%-20s ${RED}%20s${NC}\n" "zmq" "failure"
fi

